firebase.initializeApp({
    apiKey: "AIzaSyABd-UpTGR0J9ePzyOQnApbeQYv69QIUH0",
    authDomain: "redalco-d74a2.firebaseapp.com",
    projectId: "redalco-d74a2"
});
const db = firebase.firestore();
// Initialize Cloud Firestore through Firebase

/*
/     Add Organizations
*/
function addDeliveredItem() {
    //Validate all required fields
    document.querySelector('#id-item-form').checkValidity();
    document.querySelector('#item-data-form').checkValidity();

    //Get the data
    var deliveredData = [];
    deliveredData.organization = document.querySelector('#organization-name').value;
    deliveredData.date = helperGetDate().timestamp;
    deliveredData.itemType = helperContaineritemType();
    deliveredData.containerType = helperContainerTypeData();
    deliveredData.quantityDelivered = document.querySelector('#quantity-delivered').value;
    deliveredData.quantityRecieved = document.querySelector('#quantity-back').value;
    deliveredData.moneyQuantity = document.querySelector('#money-quantity').value;
    deliveredData.message = document.querySelector('#delivered-message').value;

    //Before submit check if is all valid
    if (helperIsFormValid()) {
        return;
    }

    //Send data to firebase

    db.collection("delivered").add({
        orgName: deliveredData.organization,
        date: deliveredData.date,
        itemType: deliveredData.itemType.id,
        containerType: deliveredData.containerType.id,
        quantityDelivered: deliveredData.quantityDelivered,
        quantityRecieved: deliveredData.quantityRecieved,
        moneyQuantity: deliveredData.moneyQuantity,
        message: deliveredData.message,
        enabled: 1
    }).then(function (docRef) {
        deliveredData.id = docRef.id;
        helperResetForm('#item-data-form');
        helperHideElement('#item-data-form');
        helperDisabledElement('#organization-name');
        uiAddItemRow(deliveredData);
        helperShowElement('#items-table');
        helperShowElement('#add-another-button');

    }).catch(function (error) {
        console.error("Error adding document: ", error);
    });
}

function uiAddItemRow(deliveredData) {
    // Find a <table> element with id="myTable":
    var table = document.querySelector("table");

    // Create an empty <tr> element and add it to the 1st position of the table:
    var row = table.insertRow(-1);
    row.setAttribute('firebase-id', deliveredData.id);

    // Insert new cells (<td> elements) in the "new" <tr> element:
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    var cell7 = row.insertCell(6);

    // Add the data to the new cells:
    cell1.innerHTML = deliveredData.containerType.text;
    cell2.innerHTML = deliveredData.itemType.text;
    cell3.innerHTML = deliveredData.quantityDelivered;
    cell4.innerHTML = deliveredData.quantityRecieved;
    cell5.innerHTML = '$ ' + deliveredData.moneyQuantity;
    cell6.innerHTML = "<button id='edit-item' class='btn btn-outline-primary'>Editar</button>";
    cell7.innerHTML = "<button class='btn btn-outline-danger'>Eliminar</button>";

    document.querySelector('#edit-item').addEventListener('click', async function () {
        firebaseId = this.parentElement.parentElement.getAttribute('firebase-id');
        this.blur();
        if (this.classList.contains("btn-outline-primary")) {
            var itemData = await editItem(firebaseId);
            helperShowElement('#item-data-form');
        } else {
            helperHideElement('#item-data-form');
        }
        this.classList.toggle("btn-primary");
        this.classList.toggle("btn-outline-primary");
    });
}

function editItem(firebaseId) {
    var docRef = db.collection("delivered").doc(firebaseId);
    var getDoc = docRef
        .get()
        .then(doc => {
            if (!doc.exists) {
                console.log('No such document!');
            } else {
                helperShowDataToEdit(doc.data());
                console.log('Document data:', doc.data());
            }
        })
        .catch(err => {
            console.log('Error getting document', err);
        });
    return getDoc;
}
function inputRadioSelected() {
    var radios = document.getElementsByTagName('input');
    var value;
    for (var i = 0; i < radios.length; i++) {
        if (radios[i].type === 'radio' && radios[i].checked) {
            // get value, set checked flag or do whatever you need to
            value = radios[i].value;
        }
    }
}


//Events Listeners
document.querySelectorAll('.form-control')
    .forEach(input => input.addEventListener('change', function () {
        this.classList.remove("is-invalid");
    }));

document.querySelectorAll('.form-control')
    .forEach(input => input.addEventListener('invalid', function () {
        this.classList.add("is-invalid");
    }));

document.querySelector('#add-another-button').addEventListener('click', function () {
    helperShowElement('#item-data-form');
});



firebase.initializeApp({
    apiKey: "AIzaSyABd-UpTGR0J9ePzyOQnApbeQYv69QIUH0",
    authDomain: "redalco-d74a2.firebaseapp.com",
    projectId: "redalco-d74a2"
});

// Initialize Cloud Firestore through Firebase

/*
/     Add Organizations
*/
function addOrganization() {
    var db = firebase.firestore();

    var organization = [];
    var contact = [];
    var requestBoxes = [];
    
    organization.name = document.querySelector('#organization-name').value;
    organization.rut = document.querySelector('#organization-rut').value;
    organization.street = document.querySelector('#organization-address').value;
    organization.adrressNumber = document.querySelector('#organization-address-number').value;
    organization.apartment = document.querySelector('#organization-address-apartament').value;
    contact.name = document.querySelector('#contact-name').value;
    contact.mobile = document.querySelector('#contact-mobile').value;
    contact.phone = document.querySelector('#contact-phone').value;
    contact.email = document.querySelector('#contact-email').value;
    requestBoxes.fruit = document.querySelector('#request-fruits').value;
    requestBoxes.vegetable = document.querySelector('#request-vegetables').value;

    db.collection("organization").add({
        orgName: organization.name,
        orgRUT: organization.rut,
        orgStreet: organization.street,
        orgStreetNumber: organization.adrressNumber,
        orgApartment: organization.apartment,
        contactName: contact.name,
        contactMobile: contact.mobile,
        contactPhone: contact.phone,
        contactEmail: contact.email,
        requestFruits: requestBoxes.fruit,
        requestVegetable: requestBoxes.vegetable,
        enabled: 1
    }).then(function (docRef) {
        console.log("Document written with ID: ", docRef.id);
    }).catch(function (error) {
        console.error("Error adding document: ", error);
    });
}





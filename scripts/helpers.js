function helperIsFormValid() {
    return document.querySelector('.is-invalid');
}

function helperResetForm(formId) {
    var formElement = document.querySelector(formId);
    formElement.reset();
    return true;
}

function helperHideElement(elem) {
    var formElement = document.querySelector(elem);
    formElement.setAttribute('hidden', '');
}

function helperShowElement(elem) {
    var formElement = document.querySelector(elem);
    formElement.removeAttribute('hidden');
}

function helperDisabledElement(elem) {
    var formElement = document.querySelector(elem);
    formElement.setAttribute('disabled', '');
}

function helperContaineritemType() {
    var data = [];
    var containerTypeElement = document.querySelector('#item-type');
    data.id = containerTypeElement.value;
    data.text = containerTypeElement.options[containerTypeElement.selectedIndex].text;
    return data;
}

function helperContainerTypeData() {
    var data = [];
    var containerTypeElement = document.querySelector('#container-type');
    data.id = containerTypeElement.value;
    data.text = containerTypeElement.options[containerTypeElement.selectedIndex].text;
    return data;
}

function helperGetDate() {
    var data = [];
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();

    newdate = day + "/" + month + "/" + year;

    var date = new Date();
    data.timestamp = date.getTime();
    data.formatted = newdate;

    return data;
}

function helperShowDataToEdit(dataItem){
    console.log(dataItem);
    // deliveredData.date = helperGetDate().timestamp;

    // deliveredData.organization = document.querySelector('#organization-name').value;
    // deliveredData.itemType = helperContaineritemType();
    // deliveredData.containerType = helperContainerTypeData();
    // deliveredData.quantityDelivered = document.querySelector('#quantity-delivered').value;
    // deliveredData.quantityRecieved = document.querySelector('#quantity-back').value;

    // deliveredData.moneyQuantity = document.querySelector('#money-quantity').value;
    // deliveredData.message = document.querySelector('#delivered-message').value;
    
    //document.querySelector('#organization-name').value = dataItem.orgName;
    console.log(dataItem.itemType);
    document.querySelector('#item-type').value = dataItem.itemType;
    document.querySelector('#container-type').value = dataItem.containerType
    document.querySelector('#quantity-delivered').value = dataItem.quantityDelivered;
    document.querySelector('#quantity-back').value =  dataItem.quantityRecieved;
    document.querySelector('#money-quantity').value = dataItem.moneyQuantity;
    document.querySelector('#delivered-message').value = dataItem.message;

}